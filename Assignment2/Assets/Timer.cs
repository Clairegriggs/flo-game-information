﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {
	public GUIText clock;
	public float timer = 0.0f;
	public int minutes = 0;
	public int seconds = 0;
		// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		minutes = (int) timer / 60;
		seconds = (int) timer % 60;
		clock.text = minutes + ":" + seconds;
	}
}
