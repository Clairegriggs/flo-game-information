﻿using UnityEngine;
using System.Collections;

public class BulletTimeOut : MonoBehaviour {
	
	public Rigidbody2D Flo;
	public bool reflected = false;
	public FloScript FloSc;

	void Start () {
		Destroy (gameObject, 4f);
		FloSc = FloScript.instance;
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.tag == "Player") {
			if(FloSc.shielded){
				if(FloSc.facingRight){
				   rigidbody2D.AddForce (new Vector2 (1, 0) * 3500);
				   reflected = true;
				}else{
				   rigidbody2D.AddForce (new Vector2 (-1, 0) * 3500);
				   reflected = true;
				}
			}else{
				Destroy (gameObject);
			}
		}else{
			Destroy (gameObject);
		}
	}
}