﻿using UnityEngine;
using System.Collections;

public class CrimeaFollow : MonoBehaviour {
	
	float YMargin = 1f;
	float YSmooth = 8f;
	public Vector2 maxXandY;
	public Vector2 minXandY;
	public FloScript FloSc;
	private Transform player;


	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player").transform;
	}

	void Start(){
		FloSc = FloScript.instance;
	}

	void FixedUpdate () {
		FollowFlo ();	
	}

	void FollowFlo (){
		float targetX = transform.position.x;
		float targetY = transform.position.y;

		targetX = player.position.x + FloSc.move * 4;

		if(CheckY()){
			targetY = Mathf.Lerp(transform.position.y, player.position.y, YSmooth * Time.deltaTime);
		}

		targetX = Mathf.Clamp (targetX, minXandY.x, maxXandY.x);
		targetY = Mathf.Clamp (targetY, minXandY.y, maxXandY.y);

		transform.position = new Vector3 (targetX, targetY, transform.position.z);
	}

	bool CheckY (){
		return Mathf.Abs (transform.position.y - player.position.y) > YMargin;
	}
}
