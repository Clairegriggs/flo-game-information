﻿using UnityEngine;
using System.Collections;

public class Flo : MonoBehaviour {
	
	public Transform Spawn;
	public float maxSpeed = 3.5f;
	bool facingRight = true;
	bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.2f;
	public LayerMask groundObjects;
	public float jumpHeight = 1200;
    public float acceleration = 60f;
	Animator anim;
	public bool shielded = false;
	
	void Start () {
		anim = GetComponent <Animator>();
	}

	void FixedUpdate () {
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, groundObjects);
		anim.SetBool ("ground", grounded);
		
		float move = Input.GetAxis ("Horizontal");
      
		anim.SetFloat ("speed%", (Mathf.Abs (rigidbody2D.velocity.x) / maxSpeed)  * 100f);
		anim.SetFloat ("speed", Mathf.Abs (move));
		anim.SetFloat ("vSpeed", rigidbody2D.velocity.y);

		if (Mathf.Abs (move) > 0 && Mathf.Abs (rigidbody2D.velocity.x) < maxSpeed){
		rigidbody2D.AddForce (Vector2.right * move * acceleration);
		}
		
		if (move > 0 && !facingRight)
						flip ();
				else if (move < 0 && facingRight)
						flip ();

	}
	void Update (){ 
		if (grounded && Input.GetKeyDown ("space")){
			anim.SetBool("ground", false);
			anim.SetTrigger("jump");
				rigidbody2D.AddForce(new Vector2(0,jumpHeight));
		}
		if (grounded && Input.GetKey (KeyCode.LeftShift)) {
						anim.SetBool ("slide", true);
			acceleration = 10f;
				}
		else if (!grounded && Input.GetKey (KeyCode.LeftShift)) {
			            anim.SetBool ("slide", true);
			acceleration = 10f;
			rigidbody2D.AddForce(new Vector2(0,-100));
		} 
		else {
			anim.SetBool ("slide", false);
			acceleration = 60f;
		}

		if (Input.GetKeyDown (KeyCode.S)) {
						anim.SetTrigger ("shield");
				}
		}
	
    void flip (){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		}

    void shield (){
		shielded = true;
		}
	void unshield (){
		shielded = false;
	}

/*	void OnCollisionEnter2D(Collision2D other){

		//if (Name.Equals == Bullet) {
			if (shielded) {
				Debug.Log ("shield");
			} else {
				gameObject.transform.position = Spawn.position;
				Debug.Log ("Hit");
			}
		//}
	}/*
[serializedField] list<audioClip> soundList = new List<AudioClip>();
AudioClip sound;
[SerializedField] AudioSource source;
int i = 0;
[serializedField] bool cycle = false;

void PlayFootstep (){
if (cycle){
i++;
if(i >= soundList.count){
i=0;
}
}else{
i = Random.Range(0,soundList.Count);
}
sound = soundList[i];
source.clip = sound;
source.play ();


	 */

}
