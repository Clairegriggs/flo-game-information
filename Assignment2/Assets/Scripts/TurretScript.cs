﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TurretScript : MonoBehaviour {
	
	public Rigidbody2D BulletPrefab;
	public Transform bulletSpawn;
	public Rigidbody2D Flo;
	private ArrayList triggeredColliders = new ArrayList();
	public bool FacingRight = false;
	public FloScript FloSc;
	Animator anim;


	void Start(){
		FloSc = FloScript.instance;
		anim = GetComponent<Animator>();
	}

	void Update (){
		if (FloSc.Dead) {
			CancelInvoke ("SpawnBullet");
			triggeredColliders.Clear();
		}
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.tag == "Respawn") {
			CancelInvoke ("SpawnBullet");
			Destroy (this);
			anim.SetTrigger ("splode");
		}
	}
	
	void OnTriggerEnter2D(Collider2D other){
		if (!triggeredColliders.Contains (other)) {
			triggeredColliders.Add(other);
			if (other.tag == "Player") {
				InvokeRepeating ("SpawnBullet", 0.1f, 1.2f);
			}
		}
	}
	
	void OnTriggerExit2D(Collider2D other){
		if (triggeredColliders.Contains (other)) {
			triggeredColliders.Remove (other);
			if (other.tag == "Player") {
				CancelInvoke ("SpawnBullet");
			}
		}
	}

	void SpawnBullet (){
		Rigidbody2D bulletInstance;
		bulletInstance = Instantiate (BulletPrefab, bulletSpawn.position, bulletSpawn.rotation) as Rigidbody2D;
		if (!FacingRight) {
			bulletInstance.AddForce (new Vector2 (-1, 0) * 2000);
		} else {
			bulletInstance.AddForce (new Vector2 (1, 0) * 2000);
		}
	}
}
