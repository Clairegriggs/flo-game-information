﻿using UnityEngine;
using System.Collections;

public class healthBar : MonoBehaviour {
	public float hpMax;
	public float hp;

	// Use this for initialization
	void Start () {
		hpMax = GameObject.Find ("Flo").GetComponent<FloScript> ().HP_max;

	}
	
	// Update is called once per frame
	void Update () {
		hp = GameObject.Find ("Flo").GetComponent<FloScript> ().HP;
		renderer.material.SetFloat("_Cutoff", Mathf.Lerp(0,1, 1-hp/hpMax)); 
	}
}
