﻿using UnityEngine;
using System.Collections;

public class shieldBar : MonoBehaviour {
	public float charge;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		charge = GameObject.Find ("Flo").GetComponent<FloScript> ().shieldCharge;
		renderer.material.SetFloat("_Cutoff", Mathf.Lerp(0,1, 1-charge/100)); 
	}
}
