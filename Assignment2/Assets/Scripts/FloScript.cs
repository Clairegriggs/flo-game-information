﻿using UnityEngine;
using System.Collections;

public class FloScript : MonoBehaviour {

	public static FloScript instance;

	public Transform Spawn;
	public float maxSpeed = 2f;
	public bool facingRight = true;
	bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.2f;
	public LayerMask groundObjects;
	public float jumpHeight = 1200;
    public float acceleration = 60f;
	Animator anim;
	public bool shielded = false;
	public bool Dead = false;
	public int HP_max = 3;
	public int HP = 0;
	public float shieldCharge = 100.0f;
	public float move;


	void Awake(){
		instance = this;
		HP = HP_max;
	}

	void Start () {
		anim = GetComponent <Animator>();
	}

	void FixedUpdate () {
		if (shieldCharge < 100f) {
		shieldCharge += 0.3f;
		}
		if (!Dead) {
						grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, groundObjects);
						anim.SetBool ("ground", grounded);
		
						move = Input.GetAxis ("Horizontal");
      
						anim.SetFloat ("speed%", (Mathf.Abs (rigidbody2D.velocity.x) / maxSpeed)  * 100f);
						anim.SetFloat ("speed", Mathf.Abs (move));
						anim.SetFloat ("vSpeed", rigidbody2D.velocity.y);

						if (Mathf.Abs (move) > 0 && Mathf.Abs (rigidbody2D.velocity.x) < maxSpeed) {
								rigidbody2D.AddForce (Vector2.right * move * acceleration);
						}
		
						if (move > 0 && !facingRight)
								flip ();
						else if (move < 0 && facingRight)
								flip ();
				}

	}
	void Update (){ 
		if (Input.GetKeyDown (KeyCode.R)){
			Application.LoadLevel ("level");
		}
				if (!Dead) {
						if (grounded && Input.GetKeyDown ("space")) {
								anim.SetBool ("ground", false);
								anim.SetTrigger("jump");
								rigidbody2D.AddForce (new Vector2 (0, jumpHeight));
						}
						if (grounded && Input.GetKey (KeyCode.LeftShift)) {
								anim.SetBool ("slide", true);
								acceleration = 10f;
						} else if (!grounded && Input.GetKey (KeyCode.LeftShift)) {
								anim.SetBool ("slide", true);
								acceleration = 10f;
								rigidbody2D.AddForce (new Vector2 (0, -100));
						} else {
								anim.SetBool ("slide", false);
								acceleration = 60f;
						}

						if (Input.GetKeyDown (KeyCode.S) && !shielded && shieldCharge > 50f) {
								anim.SetTrigger ("shield");
				shieldCharge -= 50;
						}
				}
		}
	
    void flip (){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		}

    void shield (){
		shielded = true;
		}
	void unshield (){
		shielded = false;
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.tag == "Respawn"){
			if (!shielded) {
				if(!Dead){
				HP--;
					anim.SetTrigger ("hurt");
				if (HP == 0){
				Dead = true;
				StartCoroutine (waitForDeath ());
				}
				}
			} 
		}
	}
	IEnumerator waitForDeath(){
		yield return new WaitForSeconds (0.5f);
		gameObject.transform.position = Spawn.position;
		rigidbody2D.velocity = new Vector3 (0, 0, 0);
		anim.SetTrigger ("spawn");
		HP = 3;
		Dead = false;
	}
}














/*
[serializedField] list<audioClip> soundList = new List<AudioClip>();
AudioClip sound;
[SerializedField] AudioSource source;
int i = 0;
[serializedField] bool cycle = false;

void PlayFootstep (){
if (cycle){
i++;
if(i >= soundList.count){
i=0;
}
}else{
i = Random.Range(0,soundList.Count);
}
sound = soundList[i];
source.clip = sound;
source.play ();

StartCoroutine (soe ());

	IEnumerator soe(){
		yield return new WaitForSeconds (1.5f);

	}
	 */
