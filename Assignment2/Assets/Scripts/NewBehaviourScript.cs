﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NewBehaviourScript : MonoBehaviour {
	
	public Rigidbody2D BulletPrefab;
	public Transform bulletSpawn;
	private ArrayList triggeredColliders = new ArrayList();

	void OnTriggerEnter2D(Collider2D other){
		if (!triggeredColliders.Contains (other)) {
			triggeredColliders.Add(other);
			if (other.tag == "Player") {
				InvokeRepeating ("SpawnBullet", 0, 2f);
			}
		}
	}
	
	void OnTriggerExit2D(Collider2D other){
		if (triggeredColliders.Contains (other)) {
			triggeredColliders.Remove (other);
			if (other.tag == "Player") {
				CancelInvoke ("SpawnBullet");
			}
		}
	}

	void SpawnBullet (){
		BulletPrefab = Instantiate (BulletPrefab, bulletSpawn.position, bulletSpawn.rotation) as Rigidbody2D;	
		BulletPrefab.AddForce (new Vector2(-1, 0) * 2000);
	}
}
